<?php

namespace Vulpes\Router;

use Vulpes\Router\Exception\RouteException;

class Router
{
    protected const URI_PATTERN = '/[^?A-Za-z0-9-_.~\/]/';
    protected const HOME_CONTROLLER = 'home';
    protected const INDEX_METHOD = 'index';
    protected const DETAILS_METHOD = 'details';
    protected const ERROR_METHOD = 'error';

    protected RouterAccess $access;

    public ?string $controller = null;
    public ?string $action = null;
    public array $params;

    public function __construct(RouterAccess $access)
    {
        $this->access = $access;
    }

    public function build(): string
    {
        return (new Builder($this->access))->exec();
    }

    public function init(): void
    {
        $this->params = $this->getRequestParams();
        $this->access->prepareParams($this->params);

        if(is_null($routes = $this->access->getRoutes())){
            $this->build();
            $routes = $this->access->getRoutes();
        }

        try {
            $this->prepareRoutes($this->params, $routes);
        } catch (RouteException $exception) {
            $this->handleRouteException($exception);
        }
    }

    protected function getRequestParams(): array
    {
        if (php_sapi_name() === 'cli') {
            return $this->requestToParams(implode('/', array_slice($_SERVER['argv'], 1)));
        }
        return $this->requestToParams(urldecode(preg_replace(self::URI_PATTERN, '',
          filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_STRING))));
    }

    protected function requestToParams(string $uri)
    {
        if (($qsPosition = strpos($uri, '?')) !== false) {
            $uri = substr($uri, 0, $qsPosition);
        }
        return array_filter(explode('/', trim($uri, "/ \t\n\r\0\x0B")), 'trim');
    }

    /**
     * @param array $params
     * @param array $router
     *
     * @throws \Vulpes\Router\Exception\RouteException
     */
    protected function prepareRoutes(array &$params, array $router): void
    {
        if (is_null($key = $this->getRouteKey($params, $router))) {
            throw new RouteException;
        }

        array_shift($params);

        if (is_object($router[$key]) === false) {
            $this->prepareRoutes($params, $router[$key]);
            return;
        }

        list($controller, $action, $requiredParams, $numberOfParams, $paramTypes) = (array)$router[$key];

        $this->controller = $controller;
        $this->action = $action;

        $this->prepareParams($requiredParams, $numberOfParams, $paramTypes);
    }

    protected function getRouteKey(array &$params, array $router): ?string
    {
        if (array_key_exists($key = strtolower(current($params)), $router)) {
            return $key;
        }
        if ($this->initHomeController($params, $router)) {
            return self::HOME_CONTROLLER;
        }
        if ($this->initIndexMethod($params, $router)) {
            return self::INDEX_METHOD;
        }
        if ($this->initDetailsMethod($params, $router)) {
            return self::DETAILS_METHOD;
        }
        return null;
    }

    protected function initHomeController(array &$params, array &$router): bool
    {
        if (array_key_exists(self::HOME_CONTROLLER, $router) === false) {
            return false;
        }
        if (is_array($router[self::HOME_CONTROLLER]) === false) {
            return false;
        }
        array_unshift($params, self::HOME_CONTROLLER);
        return true;
    }

    protected function initIndexMethod(array &$params, array $router): bool
    {
        if (0 < count($params)) {
            return false;
        }
        if (array_key_exists(self::INDEX_METHOD, $router) === false) {
            return false;
        }
        if (is_object($router[self::INDEX_METHOD]) === false) {
            return false;
        }
        array_unshift($params, self::INDEX_METHOD);
        return true;
    }

    protected function initDetailsMethod(array &$params, array $router)
    {
        if (0 === count($params)) {
            return false;
        }
        if (array_key_exists(self::DETAILS_METHOD, $router) === false) {
            return false;
        }
        if (is_object($router[self::DETAILS_METHOD]) === false) {
            return false;
        }
        array_unshift($params, self::DETAILS_METHOD);
        return true;
    }

    /**
     * @param int   $requiredParams
     * @param int   $numberOfParams
     * @param array $paramTypes
     *
     * @throws \Vulpes\Router\Exception\RouteException
     */
    protected function prepareParams(int $requiredParams, int $numberOfParams, array $paramTypes): void
    {
        if (($paramsCount = count($this->params)) < $requiredParams || $paramsCount > $numberOfParams) {
            throw new RouteException;
        }
        foreach ($paramTypes as $index => $paramType) {
            if (array_key_exists($index, $this->params) === false) {
                continue;
            }
            $this->params[$index] = $this->prepareParam($paramType, $this->params[$index]);
        }
    }

    protected function prepareParam(string $paramType, $value)
    {
        switch ($paramType) {
            case 'int':
                return intval($value);
            case 'float':
                return floatval($value);
            case 'bool':
                return boolval($value);
        }
        return $value;
    }

    protected function handleRouteException(RouteException $exception)
    {
        if (is_null($this->controller)) {
            $this->controller = $this->access->getDefaultController();
        }
        $this->action = self::ERROR_METHOD;
        $this->params = [$exception];
    }
}