<?php

namespace Vulpes\Router;

/**
 * Interface to provide accessing to router data
 * @see \Vulpes\Router\Router
 * @see \Vulpes\Router\Builder
 */
interface RouterAccess
{
    public function getRoutes(): ?array;

    public function saveRoutes(array $router): void;

    public function getControllerPaths(): array;

    public function getDefaultController(): string;

    public function prepareParams(array &$params): void;
}