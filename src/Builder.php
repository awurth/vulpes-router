<?php

namespace Vulpes\Router;

use Error;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionParameter;
use Throwable;

class Builder
{
    private const PREFIX = 'controller';
    private RouterAccess $access;

    private int $generatedCount = 0;
    private int $overwriteCount = 0;

    public function __construct(RouterAccess $access)
    {
        $this->access = $access;
    }

    public function exec(): string
    {
        $list = [];

        foreach ($this->access->getControllerPaths() as $controllerPath => $namespace) {
            $this->createControllerList($controllerPath, $controllerPath, $namespace, $currentList);
            $this->mergeControllerLists($list, $currentList);
        }

        $this->access->saveRoutes($list[self::PREFIX]);

        return __METHOD__ . " generated {$this->generatedCount}pc, with overwrite: {$this->overwriteCount}pc";
    }

    private function createControllerList(string $origPath, string $realPath, string $namespace, ?array &$list): void
    {
        $list[$key = strtolower($baseName = pathinfo($realPath, PATHINFO_BASENAME))] = [];
        foreach (glob($realPath . "/*") as $fileName) {
            if (is_dir($fileName)) {
                $this->createControllerList($origPath, $fileName, $namespace, $list[$key]);
                continue;
            }
            try {
                $this->createControllerData($origPath, $namespace, $list[$key], $fileName);
            } catch (Throwable $exception) {
                print $exception;
                exit;
            }
        }
    }

    private function createControllerData(string $origPath, string $namespace, array &$list, string $fileName): void
    {
        $className = $this->getClassName($origPath, $namespace, $fileName);
        if (is_null($reflector = $this->getReflectionClass($className))) {
            return;
        }
        $list[$key = strtolower($baseName = pathinfo($fileName, PATHINFO_FILENAME))] = [];
        foreach ($reflector->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            if ($this->isMethodUsable($method) === false) {
                continue;
            }
            $list[$key][strtolower($method->getName())] = (object)$this->getMethodDetails($method);
            $this->generatedCount++;
        }
        ksort($list[$key]);
    }

    private function getClassName(string $origPath, string $namespace, string $fileName): string
    {
        return $namespace . str_replace('/', "\\", substr($fileName, strlen($origPath), -4));
    }

    private function getReflectionClass(string $className): ?ReflectionClass
    {
        try {
            $reflector = new ReflectionClass($className);
        } catch (ReflectionException $exception) {
            return null;
        }
        if ($this->isControllerUsable($reflector)) {
            return $reflector;
        }
        return null;
    }

    private function isControllerUsable(ReflectionClass $reflector): bool
    {
        return $reflector->isAbstract() === false &&
          $reflector->isInterface() === false &&
          $reflector->isUserDefined() === true &&
          $reflector->isSubclassOf(ControllerInterface::class);
    }

    private function isMethodUsable(ReflectionMethod $method): bool
    {
        return $method->isStatic() === false &&
          $method->isAbstract() === false &&
          $method->isConstructor() === false &&
          $method->isDestructor() === false &&
          $method->isUserDefined() === true &&
          $method->getName() !== 'error' &&
          substr($method->getName(), 0, 1) !== '_';
    }

    private function getMethodDetails(ReflectionMethod $method)
    {
        return [
          $method->getDeclaringClass()->getName(),
          $method->getName(),
          $method->getNumberOfRequiredParameters(),
          $method->getNumberOfParameters(),
          $this->getParameterTypes($method)
        ];
    }

    private function getParameterTypes(ReflectionMethod $method): array
    {
        $return = [];
        foreach ($method->getParameters() as $parameter) {
            $return[] = $this->getParameterType($parameter);
        }
        return $return;
    }

    private function getParameterType(ReflectionParameter $parameter): string
    {
        if ($parameter->hasType() === false) {
            return 'string';
        }
        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        switch ($typeName = $parameter->getType()->getName()) {
            case 'string':
            case 'int':
            case 'float':
            case 'bool':
                return $typeName;
        }
        throw new Error($this->getParameterTypeError($parameter, $typeName));
    }

    private function mergeControllerLists(array &$orig, array $new): void
    {
        foreach ($new as $key => $item) {
            if (array_key_exists($key, $orig) === false) {
                $orig[$key] = $item;
                continue;
            }
            if (is_object($item) || is_object($orig[$key])) {
                $orig[$key] = $item;
                $this->overwriteCount++;
                continue;
            }
            $this->mergeControllerLists($orig[$key], $item);
        }
    }

    private function getParameterTypeError(ReflectionParameter $p, string $typeName): string
    {
        return <<<ERROR
Only built in param types (string|int|float|bool) enabled!
Info: {$p->getDeclaringClass()->getName()}::{$p->getDeclaringFunction()->getName()} (position: {$p->getPosition()}, name: {$p->getName()}, type: {$typeName})
ERROR;
    }

}