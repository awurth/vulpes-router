<?php

namespace Vulpes\Router;

use Throwable;

interface ControllerInterface
{
    public function error(Throwable $throwable): void;
}